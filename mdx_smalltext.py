import markdown
from markdown.inlinepatterns import Pattern

SMALL_RE = r'(_\.)(((?!\._).)+)\._'

class SmallPattern(Pattern):
    def handleMatch(self, m):
        el = markdown.util.etree.Element('small')
        el.text = m.group(3)
        return el

class SmallTextExtension(markdown.Extension):
    def extendMarkdown(self, md, md_globals):
        tag = SmallPattern(SMALL_RE)
        md.inlinePatterns.add('small_text', tag, '<emphasis')

def makeExtension(configs=None):
    return SmallTextExtension(configs=configs)