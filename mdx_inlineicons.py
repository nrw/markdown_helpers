import markdown
from markdown.inlinepatterns import Pattern

ICON_RE = r'(\|)([^\|]+)\2'

class IconPattern(Pattern):
    def handleMatch(self, m):
        el = markdown.util.etree.Element('i')
        el.set('class', 'big-icon big-icon-' + m.group(3))
        return el

class InlineIconsExtension(markdown.Extension):
    def extendMarkdown(self, md, md_globals):
        tag = IconPattern(ICON_RE)
        md.inlinePatterns.add('inline_icons', tag, '>emphasis')

def makeExtension(configs=None):
    return InlineIconsExtension(configs=configs)